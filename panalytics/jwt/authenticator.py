import json
import warnings
from base64 import standard_b64decode

import requests

from authenticator import Authenticator, AuthenticationException, register_authenticator


class JwtAuthenticator(Authenticator):
    def __init__(self, kwargs):
        self._url = kwargs["authUrl"]
        self._username = kwargs["username"]
        self._password = kwargs["password"]
        self._token = None

    def authenticate(self):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            try:
                r = requests.get(self._url,
                                 data=json.dumps({"email": self._username,
                                                  "password": self._password}),
                                 headers={'content-type': 'application/json'},
                                 verify=False)
            except requests.exceptions.ConnectionError:
                raise AuthenticationException()

            if r.ok:
                payload = json.loads(r.text).get("payload", None)
                if payload:
                    decoded_payload = standard_b64decode(payload).decode('utf8')
                    self._token = json.loads(decoded_payload).get("jwt", None)
                    return True
        return False

    def sign(self, message):
        if not self._token and not self.authenticate():
            raise AuthenticationException("Cannot sign: authentication failed")
        message["jwt"] = self._token
        return message


register_authenticator("jwt", JwtAuthenticator, ["username", "password", "authUrl"])