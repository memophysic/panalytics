import json
import os

import time
import warnings
from base64 import standard_b64decode
from getpass import getpass

import requests
import sys

from authenticator import get_auth_type_keys, authenticator_factory, AuthenticationException
import jwt.authenticator

_request_url_key = "requestUrl"


def validate_request_config(config):
    return config and _request_url_key in config and \
           all(key in config for key in get_auth_type_keys(config.get("authType", None)))


def get_config(terminal_args):
    config = None
    request_config_filepath = terminal_args.requestconfig or \
                              os.path.expanduser("~/.panalytics/request.json")
    try:
        with open(request_config_filepath) as req_cfg_file:
            config = json.load(req_cfg_file)

        if config.get("password") is not None:
            print("[WARNING] Configuration file contains a clear text password, "
                  "make sure this file is read protected!")
    except IOError:
        print("Could not open request config file {}".format(request_config_filepath))
    except json.decoder.JSONDecodeError:
        print("Unable to parse the config file {}".format(request_config_filepath))

    return config


def download_action(args):
    date = args.date or time.strftime("%Y-%m-%d")
    start_time = args.startTime or "00:00:00"
    end_time = args.endTime or "23:59:59"

    config = get_config(args)

    if not config:
        return False

    if "password" not in config and "username" in config:
        try:
            config["password"] = getpass("Enter the password for '{}': ".format(config["username"]))
        except EOFError:
            print("No password provided.")
            return False

    success = False
    if config and validate_request_config(config):
        authenticator = authenticator_factory(config)
        if authenticator:
            payload = ""
            try:
                payload = authenticator.sign({
                    "context": {
                        "version": 1,
                        "date": date,
                        "startTime": start_time,
                        "endTime": end_time
                    }
                })
            except AuthenticationException:
                print("Could not authenticate")

            if payload:
                r = None
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    try:
                        r = requests.get(config[_request_url_key],
                                         data=json.dumps(payload),
                                         headers={'content-type': 'application/json'},
                                         verify=False)
                    except requests.exceptions.ConnectionError:
                        print("Failed to connect to the analytics server")

                if r and r.ok:
                    payload = json.loads(r.text).get("payload", None)
                    if payload:
                        decoded_payload = standard_b64decode(payload).decode('utf8')
                        logs = json.loads(decoded_payload).get("logs", None)
                        if logs:
                            if args.output:
                                with open(os.path.expanduser(args.output), mode='w') as f:
                                    json.dump(logs, f)
                            else:
                                sys.stdout.write(json.dumps(logs))
                            success = True
                        else:
                            print("No logs found in the response")
                    else:
                        print("Received no payload from the analytics provider")

    else:
        print("Invalid request configuration")

    return success
