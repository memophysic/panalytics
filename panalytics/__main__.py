#!/usr/bin/env python3
"""
pAnalytics - Python Analytics

Query and parse analytics from an application
"""
import sys
import argparse

from download import download_action
from parse import parse_action

main_parser = argparse.ArgumentParser()

subparsers = main_parser.add_subparsers(help='Action to take')

download_parser = subparsers.add_parser("download", help="Download analytics from a server")
download_parser.add_argument("--requestconfig",
                             help="Request configuration file (defaults to ~/.panalytics/request.json)")
download_parser.add_argument("-o", "--output", help="Output file path to write to")
download_parser.add_argument("--date", help="Date for the query in YYYY-MM-DD format (defaults to today)")
download_parser.add_argument("--startTime", help="Start time for query in HH:MM:SS format (defaults to 00:00:00)")
download_parser.add_argument("--endTime", help="End time for query in HH:MM:SS format (defaults to 23:59:59)")
download_parser.set_defaults(func=download_action)

parse_parser = subparsers.add_parser("parse", help="Compute indicators from analytics")
parse_parser.add_argument("analytics", help="Analytics to parse.", nargs='?', default=sys.stdin)
parse_parser.add_argument("-g", "--group", action='store_true',
                          help="Group similar entries following the grouping configuration")
parse_parser.add_argument("-f", "--file", action='store_true',
                          help="Treat the analytics input as path to a file containing the analytics")
parse_parser.add_argument("-j", "--journey", help="Follow a user's journey using a provided userLocalId for filtering")
parse_parser.add_argument("-s", "--statistics", action='store_true', help="Print analytics statistics")
parse_parser.add_argument("--version", help="Version to query. If 'latest', the latest version available is used.")

parse_parser.set_defaults(func=parse_action)

args = main_parser.parse_args()

result = 0
try:
    result = args.func(args)
except KeyboardInterrupt:
    result = 1

sys.exit(result)
