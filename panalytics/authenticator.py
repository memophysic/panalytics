class AuthenticationException(Exception):
    pass


class Authenticator:
    def authenticate(self):
        raise NotImplementedError()

    def sign(self, message):
        raise NotImplementedError()


_authenticators = {}
""":type: dict[str, (dict) -> Authenticator]"""

_authenticators_arg_types = {}
""":type: dict[str, list[str]]"""


def register_authenticator(auth_type, authenticator_cls, arg_types):
    if issubclass(authenticator_cls, Authenticator):
        _authenticators[auth_type] = lambda kwargs: authenticator_cls(kwargs)
        _authenticators_arg_types[auth_type] = arg_types
    else:
        raise ValueError("Invalid arguments, "
                         "could not register authenticator '{}'".format(auth_type))


def authenticator_factory(kwargs):
    factory = _authenticators.get(kwargs.get("authType", None), None)
    return factory(kwargs) if factory else None


def get_auth_type_keys(auth_type):
    return _authenticators_arg_types.get(auth_type, None)
