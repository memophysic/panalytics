import glob
import json
import os
from functools import reduce
from collections import Counter

import re


def _retrieve_analytics(filepath):
    analytics = None
    try:
        with open(os.path.expanduser(filepath)) as f:
            analytics = json.load(f)
    except IOError:
        print("Could not open analytics file {}".format(filepath))
    except json.decoder.JSONDecodeError:
        print("Unable to parse the analytics file {}".format(filepath))
    return analytics


def map_by(analytics, key_getter):
    def entry_group(accumulator, entry):
        key = key_getter(entry)
        entry_list = accumulator.get(key, None)
        if entry_list:
            entry_list.append(entry)
        else:
            accumulator[key] = [entry]
        return accumulator
    return reduce(entry_group, analytics, {})


def filter_by(analytics, predicate):
    return [entry for entry in analytics if predicate(entry)]


def _compile_regex_noexcept(regex):
    try:
        compiled_regex = re.compile(regex)
        return compiled_regex
    except Exception as e:
        print("[WARNING] Invalid regex: {}".format(regex))
    return None


def create_version_predicate(version):
    return lambda entry: entry.get("version", "") == version


def create_url_match_predicate(match):
    return lambda entry: match in entry.get("url", "")


def create_url_regex_predicate(regex):
    compiled_regex = _compile_regex_noexcept(regex)
    if not compiled_regex:
        return lambda entry: False

    return lambda entry: compiled_regex.match(entry.get("url", "")) is not None


def create_url_regex_predicate_inverse(regex):
    compiled_regex = _compile_regex_noexcept(regex)
    if not compiled_regex:
        return lambda entry: False

    def ignored(entry):
        res = compiled_regex.match(entry.get("url", ""))
        return res is None

    return ignored


def print_entry_ratio(numerator, denominator, num_title, den_title):
    if numerator is None or denominator is None:
        return

    len_numerator = len(numerator)
    len_denominator = len(denominator)

    if len_denominator > 0:
        print("{}/{}: {}/{} ({:.2f} %)".format(
            num_title, den_title,
            len_numerator, len_denominator,
            100 * float(len_numerator) / float(len_denominator)))
    else:
        print("No {} during the queried period, {} {}".format(
            den_title, len_numerator, num_title))


def print_entries_per_version(entries, title, config, group=False):
    print(title, end="")
    mapped_entries = map_by(entries, lambda entry: entry.get("version"))

    has_message_query = "errorMessage" in config.get("query", {})

    if len(mapped_entries) > 0:
        print()  # endl

        if group and not config.get("group"):
            print("[WARNING] No grouping configuration for {}, printing raw entries".format(config.get("title")))
            group = False

        sorted_versions = get_sorted_version_keys(mapped_entries)
        for version, entries_per_version in ((v, mapped_entries[v]) for v in sorted_versions):
            print("  Version: {}".format(version), end="")
            if len(entries_per_version) > 0:
                print()  # endl

                if group:
                    entries_per_version = group_similar_entries(entries_per_version, config)

                for entry in entries_per_version:
                    try:
                        context = json.loads(entry["context"])
                        userLocalId = "UserLocalId: "
                        if context:
                            userLocalId += context.get("userLocalId")
                        else:
                            userLocalId += "???"
                            print("[WARNING]: Entry without context")

                        error_message = query_entry_by_name(entry, "errorMessage", config) if has_message_query else ""

                        print("=== {}\t{}{}{}\t{}".format(
                            entry.get("os", ""),
                            entry.get("url", "").split("\n")[0],
                            error_message, "\t" if error_message else "",
                            userLocalId
                        ))
                    except ValueError as e:
                        print("[WARNING] Bad entry: {}".format(entry))
            else:
                print(" (No error for this version)")
    else:
        print(" (No errors)")


def get_sorted_version_keys(map_to_sort):
    sorted_versions = [k for k in map_to_sort.keys()]
    sorted_versions.sort(key=lambda s: list(map(lambda x: int(x) if len(x) else 0,
                                                s.split("."))), reverse=True)
    return sorted_versions


def get_errors(analytics, config):
    error_entries = None
    error_regex = config.get("regex", {}).get("error")

    if error_regex:
        error_entries = filter_by(analytics, create_url_regex_predicate(error_regex))
        if error_entries:
            error_ignore_regex = config.get("ignore_regex", {}).get("error")
            if error_ignore_regex:
                error_len_before = len(error_entries)
                for regex in error_ignore_regex:
                    error_entries = filter_by(error_entries,
                                              create_url_regex_predicate_inverse(regex))
                error_len_after = len(error_entries)

                if error_len_after < error_len_before:
                    print("[INFO] Ignored '{}' entries for {}".format(error_len_before - error_len_after, config.get("title")))

    return error_entries


def group_similar_entries(entries, config):
    """
    Use filtering to group entries
    """
    grouped_entries = entries
    for grouping, patterns in config["group"].items():
        group = []
        for p in patterns:
            group += filter_by(entries, create_url_regex_predicate(p))

        if group:
            # OS stats
            os_map = Counter(entry.get("os") for entry in group)

            representative_entry = group[0]
            representative_entry["os"] = ", ".join("{}:{}".format(k, v) for k, v in os_map.items())
            representative_entry["url"] = "{}: First occurrence: '{}'".format(
                grouping,
                representative_entry["url"]
            )

            for toRemove in group:
                try:
                    grouped_entries.remove(toRemove)
                except ValueError:
                    pass  # Was not in the list, means there were duplicates

            grouped_entries.append(representative_entry)

    return grouped_entries


def get_success_vs_attempt(analytics, config):
    success_entries = None
    attempt_entries = None

    regexes = config.get("regex", {})
    success_regex = regexes.get("success")
    attempt_regex = regexes.get("attempt")

    if success_regex and attempt_regex:
        success_entries = filter_by(analytics, create_url_regex_predicate(success_regex))
        attempt_entries = filter_by(analytics, create_url_regex_predicate(attempt_regex))

    return success_entries, attempt_entries


def transform_entry(entry, key, config):
    transform = config.get("transform")
    if transform and key in transform:
        transform_function = {
            "json": lambda x: json.loads(x)
        }.get(transform[key])
        if transform_function:
            return transform_function(entry)
    return entry


def query_entry_by_name(entry, query_name, config):
    return query_entry(entry, config.get("query", {}).get(query_name), config)


def query_entry(entry, query, config):
    if not query:
        return None

    sub_entry = entry
    for key in query.split("/"):
        if key and key in sub_entry:
            sub_entry = transform_entry(sub_entry[key], key, config)
        else:
            return None
    return sub_entry


def get_users_unable_to_install(analytics, config):
    all_regexes_str = ""
    regexes = config.get("regex", {})
    success_regex = regexes.get("success")
    error_regex = regexes.get("error")
    attempt_regex = regexes.get("attempt")

    for regex in [success_regex, error_regex, attempt_regex]:
        if regex:
            if all_regexes_str:
                all_regexes_str += "|"
            all_regexes_str += "({})".format(regex)

    # Custom unity thingy
    all_project_entries = filter_by(analytics, create_url_regex_predicate(all_regexes_str))

    entries_per_user = map_by(all_project_entries, lambda entry: query_entry(entry, "user", config))
    print("Number of users: {}".format(len(entries_per_user)))
    for user, logs in entries_per_user.items():
        successes = filter_by(logs, create_url_regex_predicate(success_regex))
        errors = filter_by(logs, create_url_regex_predicate(error_regex))
        if len(successes) == 0:
            print("User '{}' never installed successfully {}".format(
                user,
                "/!\\ But didn't encounter any error!" if len(errors) == 0 else ""))


def get_analytics_by_version(analytics, version, verbose=False):
    version_mapped_analytics = map_by(analytics, lambda entry: entry.get("version"))
    sorted_versions = get_sorted_version_keys(version_mapped_analytics)

    total_entries = reduce(lambda a, b: a + float(len(b)), version_mapped_analytics.values(), 0.0)
    # TODO: Compute the actual amount of _users_, not entries
    percentage_usage = {key: 100 * float(len(entry)) / total_entries
                        for key, entry in version_mapped_analytics.items()}

    if version is not None:
        if version == "latest":
            version = sorted_versions[0]
            version_title = "{} (latest)".format(version)
        elif version in sorted_versions:
            version_title = version
        else:
            raise ValueError("The version specified is not found in the analytics")

        analytics = version_mapped_analytics[version]

        if verbose:
            print("Version: {}\tRepresents {:.2f}% of analytics entries".format(
                version_title, percentage_usage[version]))

    elif verbose:
        print("Versions:")
        for v in sorted_versions:
            print("\t{}\tRepresents {:.2f}% of analytics entries".format(v, percentage_usage[v]))

    return analytics


def get_config_files():
    config_files = []
    for parse_config_file in glob.glob(os.path.expanduser("~/.panalytics/parse/*.json")):
        with open(parse_config_file) as f:
            config = json.load(f)
            if "title" not in config:
                config["title"] = os.path.splitext(os.path.basename(parse_config_file))[0]
            config_files.append(config)
    return config_files


def parse_action(args):
    config_files = get_config_files()
    if len(config_files) < 0:
        print("No config file to parse, aborting.")
        return False

    analytics = _retrieve_analytics(args.analytics) if args.file else json.load(args.analytics)

    if not analytics:
        print("Cannot parse without valid analytics, aborting.")
        return False

    try:
        analytics = get_analytics_by_version(analytics, args.version, args.statistics)

        if args.journey:
            # Journey
            print("Following journey of user {}:".format(args.journey))
            for entry in analytics:
                context = json.loads(entry["context"])
                if not context:
                    print("[WARNING]: Entry without context")
                    continue

                if context.get("userLocalId") == args.journey:
                    print("[{}] {}: {}".format(
                        entry.get("version"),
                        entry.get("event_time"),
                        entry.get("url")))
        else:
            # Software analytics
            for config in config_files:
                print("Analytics from: {}".format(config.get("title", "(UNNAMED SOFTWARE)")))
                print("")  # endl

                print_entry_ratio(*get_success_vs_attempt(analytics, config),
                    "Success", "Attempt")

                print_entries_per_version(
                    get_errors(analytics, config),
                    "Errors",
                    config,
                    args.group)

                print("")  # endl

        return True
    except ValueError as e:
        print(e)

    return False
