# pAnalytics
Analytics grabber and parser made with Python

## Requirements
* Python 3
** requests

## Setup
The configurations are expected in a folder under your home directory: ~/.panalytics
The content should be:

.panalytics
 |
 |-(f) request.json : Configuration for download request of the analytics
 |-(d) parse
    |- Product1.json : Parsing configuration for Product1
    |- Product2.json : Parsing configuration for Product2
    |- ...

### request.json
This file is the request configuration used for the download action. The expected JSON format is:
```
{
"requestUrl": "https://analytics.myurl.com",
"authUrl": "https://myurl.to/login/from",
"authType": "jwt",
"username": "my@email.com",
"password": "MyPassword"
}
```
The password is OPTIONAL, if not provided it will be prompted.
The only authType currently supported is "jwt".
    
## Usage
### General
```
usage: panalytics [-h] {download,parse} ...

positional arguments:
  {download,parse}  Action to take
    download        Download analytics from a server
    parse           Compute indicators from analytics
```

### Download action
```
usage: panalytics download [-h] [--requestconfig REQUESTCONFIG] [-o OUTPUT]
                           [--date DATE] [--startTime STARTTIME]
                           [--endTime ENDTIME]

optional arguments:
  -h, --help            show this help message and exit
  --requestconfig REQUESTCONFIG
                        Request configuration file (defaults to ~/.panalytics/request.json)
  -o OUTPUT, --output OUTPUT
                        Output file path to write to
  --date DATE           Date for the query in YYYY-MM-DD format (defaults to today)
  --startTime STARTTIME
                        Start time for query in HH:MM:SS format (defaults to 00:00:00)
  --endTime ENDTIME     End time for query in HH:MM:SS format (defaults to 23:59:59)
```

#### Examples
Download today's analytics and save it to a file:
```python3 path\to\panalytics download -o ~\Documents\launcherStats.json```

### Parse action
```
usage: panalytics parse [-h] [-g] [-f] [-j JOURNEY] [-s] [--version VERSION]
                        [analytics]

positional arguments:
  analytics             Analytics to parse.

optional arguments:
  -h, --help            show this help message and exit
  -g, --group           Group similar entries following the grouping configuration
  -f, --file            Treat the analytics input as path to a file containing the analytics
  -j JOURNEY, --journey JOURNEY
                        Follow a user's journey using a provided userLocalId for filtering
  -s, --statistics      Print analytics statistics
  --version VERSION     Version to query. If 'latest', the latest version available is used.
```

#### Examples
Parse an analytics file for all analytics application versions:
```python3 path\to\panalytics parse -f ~\Documents\launcherStats.json```

Parse an analytics file, print their statistics and group similar entries:
```python3 path\to\panalytics parse -gs -f ~\Documents\launcherStats.json```

Parse an analytics file and follow a user's journey:
```python3 path\to\panalytics parse -f ~\Documents\launcherStats.json -j 738d1224b74a74dc30073038e5609b3d```

